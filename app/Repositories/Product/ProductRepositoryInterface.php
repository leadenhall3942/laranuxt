<?php
namespace App\Repositories\Product;

/**
 *
 * @author thangdv
 *
 */
interface ProductRepositoryInterface
{
    public function all();

    public function paginate($quantity);

    public function find($id);

    public function delete($id);

    public function save($data);
}