<?php
namespace App\Repositories\Product;

use App\Product;

/**
 *
 * @author tampt6722
 *
 */
class ProductRepository implements ProductRepositoryInterface
{

    public function all(){
        return Product::all();
    }

    public function paginate($quantity){
        return Product::paginate($quantity);
    }

    public function find($id){
        return Product::find($id);
    }

    public function save($data){
        return Product::create($data);
    }

    public function delete($id){
        Product::find($id)->delete();
        return true;
    }

    public function update($data, $id){
        $product = Product::find($id);
        $product->name = $data['name'];
        $product->price = $data['price'];
        $product->updated_at = $data['updated_at'];
        $product->save();
        return true;
    }

    public function paginating($limit, $data) {
        $page = 1;
        if(isset($data['page'])) {
            $page = $data['page'];
        }

        if(isset($data['search']) && !is_null($data['search'])) {
            $search = $data['search'];
            $searchCol = Product::where('name', 'LIKE', "%$search%")->orderBy('id', 'asc');
            $totalRecords = $searchCol->count();
            $paginatedCol = $searchCol->forPage($page, $limit)->get();
        }else {
            $productCollection = Product::orderBy('id', 'asc');
            $totalRecords = $productCollection->count();
            $paginatedCol = $productCollection->forPage($page, $limit)->get();
        }

        $lastPage = ceil($totalRecords / $limit);
        return array(
            "products" => $paginatedCol,
            "lastPage" => $lastPage,
            "totalProducts" => $totalRecords
        );
    }
}
