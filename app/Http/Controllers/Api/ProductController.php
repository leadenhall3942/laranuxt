<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Product\ProductRepository;
use Carbon\Carbon;

class ProductController extends Controller
{
    protected $_product;

    public function __construct(ProductRepository $productRepository)
    {
        $this->_product = $productRepository;
    }

    public function index(Request $request)
    {
        $data = $request->all();
        $limit = 5;
        $productCollection = $this->_product->paginating($limit, $data);

        return response()->json([
            'products' => $productCollection['products'],
            'lastPage' => $productCollection['lastPage'],
            'totalProducts' => $productCollection['totalProducts'],
            'status' => '200',
        ]);
    }


    public function create(Request $request)
    {
        $createData = [
            'name' => $request['name'],
            'price' => $request['price'],
            'created_at' => Carbon::now()
        ];
        $this->_product->save($createData);
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        $product = $this->_product->find($id);
        return response()->json([
            'data' => $product,
            'status' => '200',
        ]);
    }


    public function update(Request $request)
    {
        $productId = $request['id'];
        $updateData = [
          'name' => $request['name'],
          'price' => $request['price'],
          'created_at' => $request['created_at'],
          'updated_at' => Carbon::now()
        ];
        $this->_product->update($updateData, $productId);
        return response()->json([
            'message' => "Product is updated",
            'status' => '200',
        ]);
    }


    public function destroy($id)
    {
        $this->_product->delete($id);
        return response()->json([
            'message' => "Product is deleted",
            'status' => '200',
        ]);
    }
}
