import axios from 'axios'

export default function (context) {
  return axios.get(`https://itunes.apple.com/search?term=${context.params.key}&entity=album`)
    .then((response) => {
      context.store.commit('add', response.data.results)
    })
}
