<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api'],function(){
    Route::get('/product', 'ProductController@index')->name('product.index');
    Route::get('/product/{id}', 'ProductController@show')->name('product.product');
    Route::post('/product/update/', 'ProductController@update')->name('product.edit');
    Route::post('/product/create/', 'ProductController@create')->name('product.create');
    Route::delete('/product/delete/{id}', 'ProductController@destroy')->name('product.delete');
});
